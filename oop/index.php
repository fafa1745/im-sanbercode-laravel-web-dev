<?php 
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "<p>name: ".$sheep->namanya; // "shaun"
echo "<br>legs: ".$sheep->legs; // 4
echo "<br>cold_blooded: ".$sheep->cold_blooded.'</p>'; // "no"

$frog = new Frog("buduk");
echo "<p>name: ".$frog->namanya; // "shaun"
echo "<br>legs: ".$frog->legs; // 4
echo "<br>cold_blooded: ".$frog->cold_blooded; // "no"
echo "<br>jump: ".$frog->jump().'</p>'; // "no"

$ape = new Ape("Kera Sakti");
echo "<p>name: ".$ape->namanya; // "shaun"
echo "<br>legs: ".$ape->legs; // 4
echo "<br>cold_blooded: ".$ape->cold_blooded; // "no"
echo "<br>yell: ".$ape->yell().'</p>'; // "no"
?>